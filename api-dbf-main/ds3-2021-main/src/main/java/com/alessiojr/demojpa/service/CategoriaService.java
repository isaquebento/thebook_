package com.alessiojr.demojpa.service;

import com.alessiojr.demojpa.domain.Categoria;
import com.alessiojr.demojpa.repository.CategoriaRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class CategoriaService {

    private final Logger log = LoggerFactory.getLogger(CategoriaService.class);

    private final CategoriaRepository categoriaRepository;

    public CategoriaService(CategoriaRepository categoriaRepository) {
        this.categoriaRepository = categoriaRepository;
    }

    public List<Categoria> findAllList(){
        log.debug("Request to get All Categoria");
        return categoriaRepository.findAll();
    }

    public Optional<Categoria> findOne(Long id) {
        log.debug("Request to get Categoria : {}", id);
        return categoriaRepository.findById(id);
    }

    public void delete(Long id) {
        log.debug("Request to delete Categoria : {}", id);
        categoriaRepository.deleteById(id);
    }

    public Categoria save(Categoria categoria) {
        log.debug("Request to save Categoria : {}", categoria);
        categoria = categoriaRepository.save(categoria);
        return categoria;
    }

    public List<Categoria> saveAll(List<Categoria> categorias) {
        log.debug("Request to save Categoria : {}", categorias);
        categorias = categoriaRepository.saveAll(categorias);
        return categorias;
    }
}
