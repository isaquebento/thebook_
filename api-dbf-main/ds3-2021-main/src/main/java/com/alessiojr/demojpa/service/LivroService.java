package com.alessiojr.demojpa.service;

import com.alessiojr.demojpa.domain.Livro;
import com.alessiojr.demojpa.repository.LivroRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class LivroService {

    private final Logger log = LoggerFactory.getLogger(LivroService.class);

    private final LivroRepository livroRepository;

    public LivroService(LivroRepository livroRepository) {
        this.livroRepository = livroRepository;
    }

    public List<Livro> findAllList(){
        log.debug("Request to get All Livro");
        return livroRepository.findAll();
    }

    public Optional<Livro> findOne(Long id) {
        log.debug("Request to get Livro : {}", id);
        return livroRepository.findById(id);
    }

    public void delete(Long id) {
        log.debug("Request to delete Livro : {}", id);
        livroRepository.deleteById(id);
    }

    public Livro save(Livro livro) {
        log.debug("Request to save Livro : {}", livro);
        livro = livroRepository.save(livro);
        return livro;
    }

    public List<Livro> saveAll(List<Livro> livros) {
        log.debug("Request to save Livro : {}", livros);
        livros = livroRepository.saveAll(livros);
        return livros;
    }
}
