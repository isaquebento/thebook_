package com.alessiojr.demojpa.domain;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.AllArgsConstructor;

import java.util.Date;

import javax.persistence.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity(name = "table_livro")
public class Livro {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "titulo", length = 64)
    private String titulo;

    private int numeroPaginas;
    private String nomeAutor;
    private String nomeEditora;
    private Boolean lido;
    private Date dataLido;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "idCategoria")
    private Categoria categoria;
    
    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "idUsuario")
    private Usuario usuario;

    private Boolean isActive;

    public static Livro parseNote(String line) {
        String[] text = line.split(",");
        Livro note = new Livro();
        note.setId(Long.parseLong(text[0]));
        note.setTitulo(text[1]);
        return note;
    }
}
